/**
 * Created by luk on 8/4/15.
 */

var express = require('express');
var path = require('path');
var http = require('http');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var auth = require('http-auth');
var routes = require('./routes/routes.js');

var app = express();

// Auth
var basic = auth.basic({
    realm: "huj",
    file: "/var/www/_logger/.htpasswd"
});

app.use(auth.connect(basic));

// Fire it up (start our server)
var server = http.createServer(app).listen(3000, function() {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Listening at http://%s:%s', host, port);
});

// Initialize socket.io
var io = require('socket.io').listen(server);

// sqlite
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('logs.db');
db.run("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY, timestamp TEXT, nick TEXT, message TEXT, action TEXT)");

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

// Make our db accessible to our router
app.use(function(req, res, next) {
    req.db = db;
    next();
});

app.use('/', routes);

io.on('connection', function(socket) {
    socket.on('live', function(clientID, last_id) {
        last_id = parseInt(last_id);
        var sql_query;

        if (last_id == -1) {
            sql_query = "SELECT * FROM (SELECT * FROM logs ORDER BY id DESC LIMIT 50) WHERE id > (?) ORDER BY id ASC";
        } else {
            sql_query = "SELECT * FROM logs WHERE id > (?) ORDER by id ASC";
        }

        db.all(sql_query, [ last_id ], function(err, rows) {
            if (rows.length > 0) {
                io.emit('messages_live', clientID, rows);
            }
        });
    });

    socket.on('search', function(clientID, query) {
        var sql_query = "SELECT * FROM (SELECT * FROM logs WHERE message LIKE (SELECT '%' || (?) || '%') ORDER by id DESC LIMIT 75) ORDER by id ASC";

        db.all(sql_query, [ query ], function(err, rows) {
            io.emit('messages_search', clientID, rows);
        });
    });

    socket.on('archive', function(clientID, timestamp) {
        var sql_query = "SELECT * FROM logs WHERE timestamp > (?) AND timestamp < (?) ORDER by id ASC";

        timestamp = timestamp / 1000;

        db.all(sql_query, [ timestamp, timestamp + 86400 ], function(err, rows) {
            io.emit('messages_archive', clientID, rows);
        });
    });
});
