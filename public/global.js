/**
 * Created by LuK on 2015-07-27.
 */

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return linkify(text.replace(/[&<>"']/g, function(m) { return map[m]; }));
}

function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
}

function formatMsg(val) {
    var curr_nick = String(val["nick"]);
    var msg = escapeHtml(String(val["message"]));
    var action = String(val["action"]);
    var id = String(val["id"]);
    var date = convertToLocalTime(val["timestamp"] * 1000);
    var time = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);

    if (colors[curr_nick] === undefined) {
        colors[curr_nick] = "#"+((1<<24)*Math.random()|0).toString(16);
    }

    if (action != "message") {
        if (action == "me") {
            msg = vsprintf("%s %s", [curr_nick, msg]);
        } else if (action == "join") {
            msg = vsprintf("%s joined!", [curr_nick]);
        } else if (action == "quit") {
            msg = vsprintf("%s left!", [curr_nick]);
        } else if (action == "kick") {
            msg = vsprintf("%s has been kicked!", [curr_nick]);
        }

        curr_nick = (action == "me") ? "*" : "#titandev";
    }

    if (last_nick == curr_nick && last_action == "message") {
        curr_nick = "&nbsp;";
    }

    var message = vsprintf("<div class='row' id='%s'>" +
        "<div class='col-xs-3 col-md-2 nick' action='%s' style='%s'>%s</div>" +
        "<div class='col-xs-7 col-md-9 message'>%s</div>" +
        "<div class='col-xs-2 col-md-1 time' title='%s'>%s</div>" +
        "</div>",
        [id, action, (colors[curr_nick] !== undefined ? "color: " + colors[curr_nick] + ";" : ""), curr_nick, msg, time[1], time[2]]);

    return message;
}

function scrollToBottom() {
    $('html, body').animate({
        scrollTop: document.body.scrollHeight
    }, 2000);
}

function generateClientID() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function convertToLocalTime(timestamp) {
    var serverDate = new Date(timestamp),
        utc = serverDate.getTime() - (serverDate.getTimezoneOffset() * 60000);

    return new Date(utc);
}
