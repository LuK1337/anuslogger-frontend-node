/**
 * Created by luk on 8/4/15.
 */
var socket = io();
var clientID = generateClientID();
var colors = [];
var last_id = -1;
var last_nick = "";
var last_action = "";
var autoscroll = false;

$("#autoscroll").click(function() {
    autoscroll = !autoscroll;
    $(this).attr("class", "btn " + (autoscroll ? "btn-success" : "btn-danger"));

    if (autoscroll) scrollToBottom();
});

socket.on('messages_live', function(cid, msg) {
    if (clientID == cid) {
        // save last_id
        last_id = String(msg[msg.length - 1]["id"]);

        $.each(msg, function (key, val) {
            if ($("li#" + val["id"]).length == 0) {
                $("#messages").append(formatMsg(val));
            }

            last_nick = escapeHtml(String(val["nick"]));
            last_action = escapeHtml(String(val["action"]));
        });

        // scroll to bottom if necessary
        if (autoscroll) scrollToBottom();
    }
});

setInterval(function() {
    socket.emit("live", clientID, last_id);
}, 2500);
