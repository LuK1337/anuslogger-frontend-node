/**
 * Created by luk on 8/4/15.
 */
var socket = io();
var clientID = generateClientID();
var colors = [];
var last_nick = "";
var last_action = "";

socket.on('messages_archive', function(cid, msg) {
    if (clientID == cid) {
        $.each(msg, function (key, val) {
            if ($("li#" + val["id"]).length == 0) {
                $("#messages").append(formatMsg(val));
            }

            last_nick = escapeHtml(String(val["nick"]));
            last_action = escapeHtml(String(val["action"]));
        });
    }
});

$('#search').datepicker().on('changeDate', function(ev) {
    var date = new Date(ev.date.valueOf() + 8640000);
    var datetime = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);

    socket.emit("archive", clientID, ev.date.valueOf());
    
    $("#date").val(datetime[1]);
    $("#messages").html("");
});

$('#search').click();