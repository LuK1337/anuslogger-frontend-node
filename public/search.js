/**
 * Created by luk on 8/4/15.
 */
var socket = io();
var clientID = generateClientID();
var colors = [];
var last_nick = "";
var last_date = "";

socket.on('messages_search', function(cid, msg) {
    if (clientID == cid) {
        $("#messages").html("");

        $.each(msg, function (key, val) {
            if ($("li#" + val["id"]).length == 0) {
                var date = new Date(val["timestamp"] * 1000);
                var time = date.toISOString().match(/(\d{4}\-\d{2}\-\d{2})T(\d{2}:\d{2}:\d{2})/);

                if (last_date != time[1]) {
                    last_date = time[1];

                    $("#messages").append(vsprintf("<p class='date'>%s:</p>", [last_date]));
                }

                $("#messages").append(formatMsg(val));
            }
        });
    }
});

$("#query").keypress(function(event) {
    if (event.which == 13) {
        socket.emit("search", clientID, $("#query").val());
    }
});

$("#search").click(function() {
    socket.emit("search", clientID, $("#query").val());
});