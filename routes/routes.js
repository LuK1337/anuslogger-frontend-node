/**
 * Created by luk on 9/10/15.
 */
var express = require('express');
var router = express.Router();
var title = "#titandev";

router.get('/', function (req, res) {
    res.render("live", { title: title, subtitle: "Live" });
});

router.get('/search', function (req, res) {
    res.render("search", { title: title, subtitle: "Search" });
});

router.get('/archive', function (req, res) {
    res.render("archive", { title: title, subtitle: "Archive" });
});

router.get('/stats', function (req, res) {
    var query_1 = "SELECT nick, COUNT(nick) AS messages FROM logs WHERE `action` = 'message' GROUP BY `nick` ORDER BY `messages` DESC LIMIT 10";
    var query_2 = "SELECT (SELECT COUNT(*) from logs) as \"all_actions\"," +
        "(SELECT COUNT(*) from logs WHERE `action` = 'message') as \"messages\"," +
        "(SELECT COUNT(*) from logs WHERE `action` = 'me') as \"actions\"," +
        "(SELECT COUNT(*) from logs WHERE `action` = 'join') as \"joins\"," +
        "(SELECT COUNT(*) from logs WHERE `action` = 'quit') as \"quits\"," +
        "(SELECT COUNT(*) from logs WHERE `action` = 'kick') as \"kicks\"," +
        "(SELECT COUNT(*) FROM (SELECT nick from logs GROUP by `nick`)) as \"nicks\";";

    req.db.all(query_1, function(err_1, rows_1) {
        req.db.all(query_2, function(err_2, rows_2) {
            res.render("stats", { title: title, subtitle: "Statistics", stats: [ rows_1, rows_2] });
        });
    });
});

// catch 404 and forward to error handler
router.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
router.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        title: title,
        subtitle: "error ;_;",
        message: err.message,
        error: err
    });
});

module.exports = router;